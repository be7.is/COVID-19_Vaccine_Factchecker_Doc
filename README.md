# COVID-19 Vaccine Factchecker Document

A document containing sourced fact checks of common COVID-19 Vaccine misconceptions and claims.


## "This vaccine is not a vaccine, it is genetic enginnering that modifies your DNA"

Some "Doctors" including discredited<sup>[1]</sup> doctor Andrew Wakefield have stated that the mRNA vaccine is not a vaccine and that it modifies your DNA. Lets discect some of the things he said in a recent video circulating online<sup>[2]</sup>. 

He actually starts off well enough:

> "Messenger RNA is an intermediary between the gene and the product, the protein. It's the protein that ultimately elicits the immune response."

So far, so good. Wakefield demonstrates a baseline understanding of Cell Biology, namely that genes encode proteins via messenger RNA<sup>[3]</sup>. But he quickly goes off the rails.

> "By definition, an RNA vaccine isn't a vaccine at all because it doesn't elicit an immune response. It has to be turned into protein, and it's the protein in turn that creates the immune response."

Huh? Wakefield just said (correctly) that the RNA vaccine makes the cell produce a protein that elicits an immune response. Now, he says that the RNA vaccine isn't a real vaccine because it elicits an immune response indirectly rather than directly. This is nonsensical and self-contradictory. Regardless of mechanism, anything that intentionally elicits an immune response can be thought of as a vaccine.

> "A messenger RNA vaccine is actually genetic engineering. That's what it is."

No, it's not. This is a gigantic lie. Wakefield is purposefully misusing the term "genetic engineering," which involves the permanent alteration of an organism's genetic material. That is accomplished by changing the DNA. Messenger RNA vaccines do not do that. Furthermore, there is no biologically plausible way for them to do that.

> "What could possibly go wrong? You have cells in your own body that are producing protein to which your immune system is going to mount an immune response. That's called an autoimmune disease."

NO. This is another monumental lie. An autoimmune disease occurs when the immune system attacks your own body. The mRNA vaccines elicit an immune response that attacks the coronavirus, not your body. Wakefield is again misusing a well-understood term.

> "The potential for this to go horribly wrong is enormous."

That's true for any drug and vaccine. It's also why we do clinical trials and is what we will explore in the next claim.

This claim may have come about as a mRNA vaccine is not *technically* a typical vaccine. A typical vaccine directly stimulates the immune response. A microbe or protein injection sets off alarms, inflammation occurs, and antibodies are produced. The same eventually happens with an mRNA vaccine, but there's a crucially different first step: The mRNA must be taken up by your body cells, and then your own cells produce the protein that stimulates an immune response. As soon as the mRNA has done its job in transcribing the relevant proteins it degrades.

So far we have only talked about the mRNA vaccines, which currently are the BioNTech/Pfizer and Moderna vaccines but there is another approach to vaccination. Adenovirus. This approach _uses_ genetic enginnering to _make_ the vaccine but it does not utilise it in how it functions! 

1 : Andrew Wakefield is the disgraced former doctor who was kicked off the UK's medical register following his fraudulent research that claimed a link between the MMR vaccine and autism https://www.bmj.com/content/342/bmj.c7452. He is, in many ways, the godfather of the modern anti-vaccine movement.

2 : https://lbry.tv/@SixthSense-Truth-Search-Labs:0/Dr-Andrew-Wakefield-Warns---This-Is-Not-A-Vaccine!:b

3 : See Crash Course Biology #11 https://www.youtube.com/watch?v=itsb2SqR-R0 for a great lesson on how mRNA functions

4 : https://allianceforscience.cornell.edu/blog/2020/12/yes-some-covid-vaccines-use-genetic-engineering-get-over-it/

## "This vaccine did not go through the normal steps for regulation to test for safety and effectiveness"



## This vaccine is using untested experimental technology"

Like all scientific breakthroughs they are built on the shoulders of the giants befire them.


## "We don't know the long term effects"


## "There have been reports of adverse reactions"


## "The vaccine is much less effective when only one dose is taken"


## "Pharmacutical companies do not have our nest intrest at heart"

> Another big difference is that governments agreed to pay companies to produce large amounts of their vaccines in advance, even while trials were testing how well the vaccine actually worked in people. If those clinical trials showed a vaccine didn’t work, or had unacceptable side effects, it would be thrown out –  but the company wouldn’t lose money

## "Many Doctors have spoken out against the vaccine"
Sucharit Bhakdi, Jane Orient, Vladamir Zelenko
 
 https://thehighwire.com/videos/the-biggest-experiment-ever-done/ @ 14mins

 pushing hypoquine
# Sources Review

* Crash Course: An award winning, highly well sourced educational resource that is approachable by people of all educational levels.

* British Medical Journal (BMJ)

* 